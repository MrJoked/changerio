<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Extensions\MemcachedStore;
use Illuminate\Support\Facades\Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Cache::extend('memcached', function ($app) {
//            return Cache::repository(new MemcachedStore);
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
