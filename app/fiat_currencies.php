<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class fiat_currencies extends Model
{
	use Searchable;

	protected $fillable=['currency_key', 'currency_symbol', 'currency_name', 'currency_symbol_native','currency_decimal_digits', 'currency_rounding', 'currency_code', 'currency_icon_name', 'currency_name_plural'];
    //
  	public function searchableAs()
  	{
    	return 'fiat_currencies_index';
  	}
}
