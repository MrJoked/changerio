<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class crypto_currencies extends Model
{
 	use Searchable;
 	protected $fillable=[    'currency_key', 'currency_symbol', 'currency_name', 'currency_symbol_native','currency_decimal_digits', 'currency_rounding', 'currency_code', 'currency_name_plural', 'currency_icon_name','currency_rank','currency_id','currency_buysellurl','currency_exchangeurl'];
 	public function searchableAs()
  	{
    	return 'crypto_currencies_index';
  	}
}
