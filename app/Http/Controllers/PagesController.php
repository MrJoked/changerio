<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\fiat_currencies;
use App\crypto_currencies;
use Khill\Lavacharts\Lavacharts;
use App\LavaDate;
use DateTime;
use App\Http\Controllers\RestApiController;
use Carbon\Carbon;


class PagesController extends Controller
{
	const __apiEndPoint = "http://localhost/api";
	const __cryptoComparEndpoint = "https://min-api.cryptocompare.com/data/histoday";


    /**
     * Just rendering Main page
    */
	public function Mainpage_render(){
		$liveStream = self::LiveStream();
        $recent_all = self::getRecentConvertion(18);
		$recentconversions = (!empty($recent_all))?array_chunk($recent_all,round(count($recent_all)/2)):[];
		$leftRecent = ($recentconversions)?$recentconversions[0]:[];
		$rightRecent = ($recentconversions)?$recentconversions[1]:[];
		$fiat = self::getFiatCurrencies(15);
		$crypto = self::getCryptoCurrencies(15);
		//dd($crypto);dd($fiat); die();
		return view('static.mainpage',['breadcrumbs' => $this->breadcrumbs,
									   'liveStream' => $liveStream,
									   'leftRecent' => $leftRecent,
									   'rightRecent'=> $rightRecent,
									   'fiat'		=> $fiat,
									   'crypto'		=> $crypto
									]);
	}


	public function getRecentConvertion(Int $int){
		$api_path = self::__apiEndPoint."/rates/recent";

        $request = Request::create($api_path, 'GET');
        $json_data = json_decode(Route::dispatch($request)->getContent(), true);

		$coinData = (!empty($json_data))?(array)$json_data:['searches'=>[]];
        $json_fiat = File::get(public_path('fiat-currencies.json'));
        $fiat = json_decode($json_fiat,true);
        $json_crypto = File::get(public_path('crypto-currencies.json'));
        $crypto = json_decode($json_crypto,true);
		for($i=0; $i < count($coinData['searches']); $i++){
//			preg_match('/([A-Z,0-9]{1,8})\/([A-Z,0-9]{1,8})\/([0-9]{1,20})/', $coinData['searches'][$i]['query'], $matches);
			$matches[1] = strtoupper($coinData['searches'][$i]['from']);
			$matches[2] = strtoupper($coinData['searches'][$i]['to']);
			$matches[3] = $coinData['searches'][$i]['sum'];

			/* define array of from convert from */
			if(array_key_exists ( $matches[1], $fiat )){
				$coinData['searches'][$i]['fromType'] = 'fiat';
				$coinData['searches'][$i]['fromurl'] = $matches[1];
				}
			else{$coinData['searches'][$i]['fromType'] = 'crypto';
				 $coinData['searches'][$i]['fromurl'] = $crypto[$matches[1]]['icon_name'];
				}
			/* define array of from convert to */
			if(array_key_exists ( $matches[2], $fiat )){
				$coinData['searches'][$i]['toType'] = 'fiat';
				$coinData['searches'][$i]['tourl'] = $matches[2];
			}
			else{$coinData['searches'][$i]['toType'] = 'crypto';
				  $coinData['searches'][$i]['tourl'] = $crypto[$matches[2]]['icon_name'];
				}
			// Parse createdAt field
			$format = '%Y-%m-%d %H:%m:%S';
			$tmp = strptime($coinData['searches'][$i]['createdAt'], $format);

			$happy_day = new \DateTime($tmp['tm_year'].'-'.$tmp['tm_mon'].'-'.$tmp['tm_mday'].' '.$tmp['tm_hour'].':'.$tmp['tm_min']);
			//dd($happy_day);die();
			$now = new \DateTime('now');
			$coinData['searches'][$i]['Interval'] = $now->diff($happy_day);
		}
		$coinData = ($coinData['searches'])?array_slice($coinData['searches'], 0, $int):[];		
		//dd($coinData); die();
		return (!empty($coinData))?$coinData:[];
	}


	public function getFiatCurrencies(Int $int){
		$currencies = fiat_currencies::all()->take($int);
		return $currencies;
	}
	public function getCryptoCurrencies(Int $int){
		$currencies = crypto_currencies::all()->take($int);
		return $currencies;
	}

	public function LiveStream(){
		$api_path = self::__apiEndPoint."/rates";
        $request = Request::create($api_path, 'GET');
        $json_data = json_decode(Route::dispatch($request)->getContent());
        $coinData = $json_data->quotes;
		return ($coinData)?(array)$coinData:[];
	}


	public function Terms_render(){
		$this->breadcrumbs->addCrumb('Home', '/');
		$this->breadcrumbs->addCrumb(trans('content.terms_title'), '/terms');  
		return view('static.terms',['breadcrumbs' => $this->breadcrumbs]);
	}

	public function Privacy_render(){
		$this->breadcrumbs->addCrumb('Home', '/');
		$this->breadcrumbs->addCrumb(trans('content.privacy_title'), '/privacy');  
		return view('static.privacy',['breadcrumbs' => $this->breadcrumbs]);
	}

	public function Contact_render(){
		$this->breadcrumbs->addCrumb('Home', '/');
		$this->breadcrumbs->addCrumb(trans('content.contact_title'), '/contact');  
		return view('static.contact',['breadcrumbs' => $this->breadcrumbs]);
	}

	public function Convert_render(Request $request, string $from = 'XRP', string $to = 'USD', string $sum = '99'){
		$name = Route::currentRouteName();
		$subtitle = trans('content.subtitle_'.$name);

		$this->breadcrumbs->addCrumb('Home', '/')
						  ->addCrumb(ucfirst($name), '/'.$request->route()->getName());
		if(isset($from)){ $this->breadcrumbs->addCrumb(strtoupper($from), strtolower($from)); }
		if(isset($from)){ $this->breadcrumbs->addCrumb(strtoupper($from).' to '.strtoupper($to), strtolower($from).'/'.strtolower($to)); }

		$api_path = self::__apiEndPoint."/rates";//.$from."/".$to."/".$sum;
        $request = Request::create($api_path, 'GET');
        $jsonData = (!empty($request))?json_decode(Route::dispatch($request)->getContent()):['result'=>1];
		//$convertData = (!empty($json_data))?json_decode($json_data,true):['result'=>1];

        //If we`re converting fiat
		$rate_from = $rate_to = 1;
        foreach($jsonData->quotes as $key => $rate){
            if(strtolower($key) == $from) { $rate_from = $rate->rate; }
            if(strtolower($key) == $to) { $rate_to = $rate->rate; }
        }
        // If we need to convert crypto(825 = usd_id)
        $api_coinmarket_path = "https://api.coinmarketcap.com/v2/ticker/825";
        $api = new RestApiController();
        if($rate_from == 1 && $from !='usd'){
              $rate_from = $api->getConvertRangeTousd($from,$api_coinmarket_path);
        }
        if($rate_to == 1 && $to !='usd'){
              $rate_to = $api->getConvertRangeTousd($to,$api_coinmarket_path);
        }

        $convertData['result'] = ($rate_from/$rate_to)*$sum;

		$ratiofromto = round($convertData['result']/$sum,8);
		$ratiotofrom = round($sum/$convertData['result'],8);

		$from = strtoupper($from);
		$to = strtoupper($to);

		$fromSelect = fiat_currencies::where('currency_key', '=', $from)->get();		
		$fromtype = 'f';
		if($fromSelect->count() == 0){
			$fromSelect = crypto_currencies::where('currency_key', '=', $from)->get();
			$fromtype = 'c';
		};

		$toSelect = fiat_currencies::where('currency_key', '=', $to)->get();		
		$totype = 'f';
		if($toSelect->count() == 0){
			$toSelect = crypto_currencies::where('currency_key', '=', $to)->get();
			$totype = 'c';
		};
		//echo $fromtype." ".$totype." ".$from." ".$to; die();
		$recentconversions = self::getRecentConvertion(28);
		$btctofiat = self::getRandomCurrenciesPair(12,'fiat');
		$btctocrypto = self::getRandomCurrenciesPair(12,'crypto');
		$fiattobtc = self::getRandomCurrenciesPair(12,'fiat');
		$cryptotobtc = self::getRandomCurrenciesPair(12,'crypto');
		$popularAmounts = self::getPopularAmounts();
		if($fromtype == 'c' ||  $totype == 'c') {
		    $rates = self::getChart($from,$to);
		}
		else{
		    $rates = '';
		}
        /**
         * Put recent conversion info to cache
         */
        $current_conversion = ['from' => strtolower($from), 'to' => strtolower($to), 'sum' => $sum, 'createdAt'=>date('Y-m-d H:m:s')];
        $expiresAt = Carbon::now()->addHours(15*24);
        for($i=1;$i<16;$i++){
            if (Cache::has('recent_conversion' . $i)) {
                continue;
            } else {
                Cache::put('recent_conversion' . $i, $current_conversion, $expiresAt);
                break;
            }
        }
       // Cache::flush();
		return view('static.converter',['breadcrumbs' => $this->breadcrumbs,
										'routename'	=> $name,
										'subtitle'	=> $subtitle,
										'from' 		=> $from,
										'to' 		=> $to,
										'sum' 		=> $sum,
										'fromSelect'=> $fromSelect->first(),
										'toSelect'	=> $toSelect->first(),
										'fromType'	=> $fromtype,
										'toType'	=> $totype,
										'result'	=> $convertData['result'],
										'ratiofromto'		=> $ratiofromto,
										'ratiotofrom'		=> $ratiotofrom,
										'recent'	=> $recentconversions,
										'btctofiat' => $btctofiat,
										'btctocrypto' => $btctocrypto,
										'fiattobtc' => $fiattobtc,
										'cryptotobtc' => $cryptotobtc,
										'popfrom'  =>	$popularAmounts[0],
										'popto' 	=>	$popularAmounts[1],
										'popsum' 	=>	$popularAmounts[2],
										'rates'		=> 	$rates,
										]);
	}

	public function Currency_render(Request $request, String $currency){
		$api_path = "https://api.coinmarketcap.com/v1/ticker";
		$this->breadcrumbs->addCrumb('Home', '/');
		$this->breadcrumbs->addCrumb(trans('content.currency_title'), '/currency'); 
		$title = trans('content.currency_title_'.$currency);
		$symbol = trans('symbols.'.$currency);		

		$api_request = $api_path."/?convert=".$currency."&limit=0";
		$code = "coinmarket_".md5($api_request);	
		if ( false === ( $coinData = unserialize(Cache::get($code)) ) ) {	
			$json_data = @file_get_contents($api_path."/?convert=".$currency."&limit=0");
			$coinData = json_decode($json_data,true);	

			Cache::add($code, serialize($coinData), 1);
		}
		$data = array_slice($coinData, 0, 100);
		if ($request->ajax()){
			$data = array_slice($coinData, $request->num, 100, true);
			return response()->json(['success'=>$data]);
		}
		return view('static.currency', ['breadcrumbs' => $this->breadcrumbs,
									    'title' 	=> $title,
										'id'		=> $currency,
										'currencies'=> $data,
										'symbol'	=> $symbol
									]);
	}

	public function getPopularAmounts(){
		$popularAmounts = [0.001, 0.01, 0.1, 1, 10, 100, 1000, 10000];
		$from = crypto_currencies::where('currency_key','=','BTC')->get();
		$to = fiat_currencies::where('currency_key','=','EUR')->get();
		//dd($from); die(); 
		return [$from, $to, $popularAmounts];
	}

	public function getRandomCurrenciesPair(Int $int, String $currencytype){
		switch($currencytype){
			case 'crypto': $currencies = crypto_currencies::inRandomOrder()->take($int)->get();
				  break;
			case 'fiat': $currencies = fiat_currencies::inRandomOrder()->take($int)->get();
				  break;
		}
		return $currencies;
	}

	public function getChart($from, $to){
		$api_path = self::__cryptoComparEndpoint."?fsym=".$from."&tsym=".$to."&limit=15&aggregate=1";
		$json_data = @file_get_contents($api_path);
		$chartData = json_decode($json_data,true);
		//dd($chartData); die();
		$rates = \Lava::DataTable();
		$rates->addStringColumn('Date')
               ->addNumberColumn($from);

        $rates_val = [];
        if($chartData['Response']=="Success"){
        	foreach($chartData['Data'] as $chartPoint){
          		$rates->addRow([date('d M', $chartPoint['time']), $chartPoint['close']]);
           		$rates_val[] = $chartPoint['close'];
        	}

        	\Lava::ColumnChart('Rates', $rates, [
    			'title' 	=> '',
    			'pointSize' => 6,
    			'maxValue'	=> 0.07,
    			'scales'	=> [
       				 'yAxes' => ['ticks' => ['min' => 0, 'max' => 0.07]]
      			],
                'bar'=> [
                    'groupWidth' => '75%'
                ],
    	    	'vAxis' => [
        			'title' => 'USD',
        			'textPosition' => 'out',
        			'baseline'	=> 1,
        			'baselineColor' =>	'#000',
        			'viewWindowMode' => 'explicit',
            		'viewWindow' => [
              				'max'=> max($rates_val),
              				'min'=> min($rates_val)
            		],
                    'format' => 'currency',
                    'gridlines' => [
                        'count' => 5
                    ]
    			],
				'hAxis'	=> [
					'title' => 'Dates',
					'textPosition' => 'out',
					'baseline'	=> 1,
					'baselineColor' =>	'#000',
                    'format' => 'string',
				]
			]);
        return $rates->toJson();
        }
        return '';
	}


}
//changerio96)-
