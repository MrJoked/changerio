<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Creitive\Breadcrumbs\Breadcrumbs;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $breadcrumbs;

  /**
   * Создание построителя нового профиля.
   *
   * @param  UserRepository  $users
   * @return void
   */
  public function __construct()
  {
      $this->breadcrumbs = new Breadcrumbs;
  }

    /**
   * Привязка данных к представлению.
   *
   * @param  View  $view
   * @return void
   */
  public function compose(View $view)
  {
    $view->with('breadcrumbs', $this->breadcrumbs);
  }
}
