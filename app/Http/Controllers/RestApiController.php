<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;

class RestApiController extends Controller
{
    const __apiEndPoint = "http://localhost/api";
    const __cryptoComparEndpoint = "https://min-api.cryptocompare.com/data/histoday";

    /**
     * Gets rates from cache, saved there from apilayer.net
     */
    public function getRatefromCache($date = null){
        $api_path = "http://apilayer.net/api/".((empty($date))?'live':'historical')."?access_key=8c8586da40ee12cdaae638063e3f208b&format=1".((empty($date))?'':'&date='.$date);
        $json_data = @file_get_contents($api_path);
        $rates = (!empty($json_data))?json_decode($json_data,true):[];
        foreach ($rates['quotes'] as $key => $rate){
            $rates['quotes'][substr($key, -3, 3)] = $rate;
            unset($rates['quotes'][$key]);
        }
        return $rates;
    }

    /**
     * Get rates for living rates and others structure blocks
     */
    public function get_api_up(Request $request){
        $expiresAt = Carbon::now()->addHours(24);
        $expiresAt_old = Carbon::now()->addHours(2*24);

        $currency_rates_old = Cache::remember('apilayer_info_old', $expiresAt_old, function() {
            $yesterday  = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
            return $this->getRatefromCache(date('Y-m-d', $yesterday));
        });
        $currency_rates_new = Cache::remember('apilayer_info', $expiresAt, function() {
            return $this->getRatefromCache();
        });


        $currency_rates = $currency_rates_new;
        $currency_rates['quotes'] = [];
        foreach ($currency_rates_new['quotes'] as $key => $rate){
            $rate = 1/$rate;
            $currency_rates['quotes'][$key] = [];
            $currency_rates['quotes'][$key]['rate'] = $rate;
            // $rates['name'] = $rate[''];
            foreach ($currency_rates_old['quotes'] as $key_old => $rate_old){
                $rate_old = 1/$rate_old;
                if ($key_old == $key){
                    $currency_rates['quotes'][$key]['percent'] = ($rate_old - $rate)*100/$rate_old;
                }
            }
        }
        return $currency_rates;
    }

    /**
     * Get recent conversions array
    */
    public function get_recent_api(Request $request){
        $return = [];

        for($i=1;$i<16;$i++){
            if (Cache::has('recent_conversion' . $i)) {
                $return['searches'][] = Cache::get('recent_conversion' . $i);
            }

        }
        return json_encode($return);
    }


    public function getConvertRangeTousd($currency,$api_path){
        $json_data = @file_get_contents($api_path."/?convert=".$currency);
        $coinData = json_decode($json_data,true);
        return (!empty($coinData['data']['quotes'][strtoupper($currency)] ) )? 1/$coinData['data']['quotes'][strtoupper($currency)]['price']:1;
    }
}