<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\fiat_currencies;
use App\crypto_currencies;

class CurrenciesListsController extends Controller
{
    //
    public function fiat_currencies(Request $request){ 
          $this->breadcrumbs->addCrumb('Home', '/');
          $this->breadcrumbs->addCrumb('Fiatcurrencies', '/fiatcurrencies');   	
    	    $currencies = fiat_currencies::all()->take(100);   	
		      return view('static.fiatcurrencies', ['currencies'=>$currencies, 'breadcrumbs' => $this->breadcrumbs]);
    }

    public function crypto_currencies(Request $request){    
          $this->breadcrumbs->addCrumb('Home', '/');
          $this->breadcrumbs->addCrumb('Cryptocurrencies', '/cryptocurrencies');  
          $currencies = crypto_currencies::all()->take(100);    
          return view('static.cryptocurrencies', ['currencies'=>$currencies, 'breadcrumbs' => $this->breadcrumbs]);
    }
    /***************** Adding by ajax next 100 currencies *************/
    
    public function fiat_currenciesadd(Request $request){
        if(isset($request->num) &&  $request->num!='all'){
    	    $currencies = fiat_currencies::all()->slice($request->num, 100);
        }
        elseif(isset($request->num) && $request->num=='all'){
           $currencies = fiat_currencies::all();
         }
  		  return response()->json(['success'=>$currencies]);
    }

    public function crypto_currenciesadd(Request $request){     
      if(isset($request->num) &&  $request->num!='all'){
          $currencies = crypto_currencies::all()->slice($request->num, 100);
        }
        elseif(isset($request->num) && $request->num=='all'){
           $currencies = crypto_currencies::all();
         }
      return response()->json(['success'=>$currencies]);
    }

    public function Searchcurrency(Request $request){
        $crypto = crypto_currencies::search($request->search)->get();
        //$count_crypto = count()
        $fiat = fiat_currencies::search($request->search)->get();
        $num = count($fiat);
        //dd($crypto->merge($fiat)); die();
        return [$fiat->union($crypto),$num];
    }
}
