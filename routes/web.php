<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', 'PagesController@Mainpage_render')->name('mainpage');//function () {

Route::get('/cryptocurrencies', 'CurrenciesListsController@crypto_currencies')->name('cryptocurrencies');
Route::post('/cryptocurrenciesadd', 'CurrenciesListsController@crypto_currenciesadd')->name('cryptocurrenciesadd');

Route::get('/fiatcurrencies', 'CurrenciesListsController@fiat_currencies')->name('fiatcurrencies');
Route::post('/fiatcurrenciesadd', 'CurrenciesListsController@fiat_currenciesadd')->name('fiatcurrenciesadd');

Route::get('/terms', 'PagesController@Terms_render')->name('terms');

Route::get('/privacy', 'PagesController@Privacy_render')->name('privacy');

Route::get('/contact', 'PagesController@Contact_render')->name('contact');

Route::get('/currency/{currency}', 'PagesController@Currency_render')->name('currency');
Route::post('/currency/{currency}', 'PagesController@Currency_render')->name('currencyajax');

Route::get('/converter/{from}/{to}/{sum}', 'PagesController@Convert_render')->name('converter');
Route::get('/converter/{from?}', 'PagesController@Convert_render')->name('converter');

Route::get('/convert/{from?}', 'PagesController@Convert_render')->name('convert');
Route::get('/calculator/{from?}', 'PagesController@Convert_render')->name('calculator');
Route::get('/exchange/{from?}', 'PagesController@Convert_render')->name('exchange');

Route::post('/searchcurrency', 'CurrenciesListsController@Searchcurrency')->name('search');

Route::get('setlocale/{locale}', function ($locale) {
    
    if (in_array($locale, \Config::get('app.locales'))) {   
    	Session::put('locale', $locale);                   
    }

    return redirect()->back();

})->name('setlocale');