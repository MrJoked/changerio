
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Meta Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'previous' => '&laquo; Previous',
    'next' => 'Next &raquo;',
    'title' => 'Changerio',
    'company_name' => 'Changerio',
    'home_title' => 'Cryptocurrency Converter',
    'cryptocurrencies_title' => 'List of Cryptocurrencies',
    'fiatcurrencies_title' => 'List of Fiat Currencies',
    'ico_title' => 'The Best Initial Coin Offering List',
    'terms_title' => 'Terms of Use',
    'privacy_title' => 'Privacy Policy',
    'contact_title' => 'Contact Us',
    'currency_title' => 'All Cryptocurrencies Prices',
    'home_description' => 'Cryptocurrencies and fiat currency conversion rate online calculator. Calculate crypto to fiat exchange rates with updated prices.',
    'cryptocurrencies_description' => 'List of all traded cryptocurrencies name with short code in the world. You can calculate cryptocurrency to fiat currency conversion rates.',
    'fiatcurrencies_description' => 'List of all traded fiat currencies name with ISO code in the world. You can calculate fiat to cryptocurrency conversion rates.',
    'ico_description' => 'Popular ICO List examines present and up and coming Initial Coin Offerings, which we offer as a curated and dependably up and coming rundown of inclining and promising ICOs.',
    'terms_description' => 'By using this Site you indicate that you accept the following terms and conditions. If you do not agree to these terms of use, please do not continue to use changerio.com',
    'privacy_description' => 'Changerio we are dedicated to safeguarding and preserving your privacy when visiting our site or communicating by email with us.',
    'contact_description' => 'Changerio has range of advertising options. If you wish to advertise with us, please contact us via email and our representative shall contact you.',
    'currency_description' => 'Check out all and compare all Cryptocurrencies prices on one platform in BTC (Bitcoin), ETH (Ethereum), USD, EUR, GBP and INR.',
    'home_keywords' => 'cryptocurrency conversion rate, cryptocurrency converter, cryptocurrency exchange rates, crypto to fiat rates, crypto to crypto exchange rates, fiat to crypto converter, cryptocurrency calculator',
    'title_company' => 'Changerio',
    'title_common_to' => 'to',
    'title_converter_part1' => 'Converter',
    'title_converter_part2' => 'Conversion Rate',
    'title_convert_part1' => 'Conversion',
    'title_convert_part2' => 'Price in',
    'title_calculator_part1' => 'Calculator',
    'title_calculator_part2' => 'Exchange Rate',
    'title_exchange_part1' => 'Exchanger',
    'title_exchange_part2' => 'Exchange Rate',
    'title_converter_com1' => 'Exchange Rate',
    'title_converter_com2' => 'Converter',
    'title_converter' => 'AMOUNT FROM_CODE to TO_CODE Converter | FROM_NAME Conversion Rate - Changerio',
    'title_convert' => 'AMOUNT FROM_CODE to TO_CODE Conversion | FROM_NAME Price in TO_CODE - Changerio',
    'title_calculator' => 'AMOUNT FROM_CODE to TO_CODE Calculator | FROM_NAME Exchange Rate - Changerio',
    'title_exchange' => 'AMOUNT FROM_CODE to TO_CODE Exchanger | FROM_NAME Exchange Rate - Changerio',
    'title_converter_1' => 'FROM_NAME Exchange Rate (FROM_CODE) | FROM_NAME Converter - Changerio',
    'title_converter_2' => 'FROM_CODE to TO_CODE Converter | FROM_NAME - Changerio',
    'description_converter' => 'AMOUNT FROM_CODE to TO_CODE Conversion Rates. Convert from AMOUNT FROM_NAME to TO_NAME get exchange rate of this pair. Changerio online converter shows how much is AMOUNT FROM_NAME in TO_NAME. Cryptocurrencies & Fiat currency converter - Changerio',
    'description_convert' => 'AMOUNT FROM_CODE to TO_CODE Convert Rates. Convert from AMOUNT FROM_NAME to TO_NAME get exchange rate of this pair. Changerio online converter shows how much is AMOUNT FROM_NAME in TO_NAME. Cryptocurrencies & Fiat currency converter - Changerio',
    'description_exchange' => 'AMOUNT FROM_CODE to TO_CODE Exchange Rates. Exchange from AMOUNT FROM_NAME to TO_NAME get exchange rate of this pair. Changerio online exchanger shows how much is AMOUNT FROM_NAME in TO_NAME. Cryptocurrencies & Fiat currency exchanger - Changerio',
    'description_calculator' => 'AMOUNT FROM_CODE to TO_CODE Exchange Rates. Calculate from AMOUNT FROM_NAME to TO_NAME get exchange rate of this pair. Changerio online calculator shows how much is AMOUNT FROM_NAME in TO_NAME. Cryptocurrencies & Fiat currency calculator - Changerio',
    'og_locale' => 'en_US'
];