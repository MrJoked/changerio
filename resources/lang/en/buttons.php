<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'convert_button' => 'CONVERT',
    'buysell_button' => 'BUY / SELL',
    'exchange_button' => 'EXCHANGE',
    'active_button' => 'ACTIVE',
    'upcoming_button' => 'UPCOMING',
    'previous_button' => 'PREVIOUS'
];