
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Header Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'currencies' => 'Currencies',
    'language' => 'Language',
    'search_box_text' => 'Search Currency',
    'home' => 'Home',
    'cryptocurrencies' => 'Cryptocurrencies',
    'fiatcurrencies' => 'Fiat Currencies',
    'ico' => 'ICO',
    'bc_convert' => 'Convert',
    'bc_converter' => 'Converter',
    'bc_calculator' => 'Calculator',
    'bc_exchange' => 'Exchange'
  ];