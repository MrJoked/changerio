
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Meta Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

        'title' => 'Changerio',
        'company_name' => 'Changerio',
        'home_title' => 'Convertisseur de Crypto-monnaie',
        'cryptocurrencies_title' => 'Liste des cryptomonnaies',
        'fiatcurrencies_title' => 'Liste des devises Fiat',
        'ico_title' => 'La meilleure liste initiale de pièces de monnaie',
        'terms_title' => 'Conditions d`utilisation',
        'privacy_title' => 'Politique de confidentialité',
        'contact_title' => 'Contactez nous',
        'currency_title' => 'Tous les prix Cryptomonnaies',
        'home_description' => 'Cryptomonnaies et calculateur en ligne de taux de conversion de monnaie fiat. Calculer les taux de change crypto à fiat avec les prix mis à jour.',
        'cryptocurrencies_description' => 'Liste de tous les noms cryptomonnaies échangés avec code court dans le monde. Vous pouvez calculer la crypto-monnaie aux taux de conversion des devises fiat.',
        'fiatcurrencies_description' => 'Liste de tous les monnaies fiduciaires échangé nom avec le code ISO dans le monde. Vous pouvez calculer les taux de conversion fiat en crypto-monnaie.',
        'ico_description' => 'La liste ICO la plus populaire examine les offres de pièces initiales actuelles et futures, que nous vous proposons en tant qu`organisation organisée, fiable et en pleine ascension d`ICO prometteurs et prometteurs.',
        'terms_description' => 'En utilisant ce site, vous indiquez que vous acceptez les termes et conditions suivants. Si vous n`acceptez pas ces conditions d`utilisation, ne continuez pas à utiliser changerio.com',
        'privacy_description' => 'Changerio nous sommes dédiés à la sauvegarde et la préservation de votre vie privée lorsque vous visitez notre site ou communiquez par email avec nous.',
        'contact_description' => 'Changerio a une gamme d`options de publicité. Si vous souhaitez faire de la publicité avec nous, s`il vous plaît contactez-nous par e-mail et notre représentant vous contactera.',
        'currency_description' => 'Découvrez tous et comparez tous les prix Cryptomonnaies sur une plateforme en BTC (Bitcoin), ETH (Ethereum), USD, EUR, GBP et INR.',
        'home_keywords' => 'taux de conversion de crypto-monnaie, convertisseur de crypto-monnaie, taux de change de cryptomonnaie, crypto-rate, taux de change crypto-crypto, convertisseur fiat-crypto, calcul de crypto-monnaie',
        'title_company' => 'Changerio',
        'title_common_to' => 'à',
        'title_converter_part1' => 'Convertisseur',
        'title_converter_part2' => 'Taux de conversion',
        'title_convert_part1' => 'Conversion',
        'title_convert_part2' => 'Prix en',
        'title_calculator_part1' => 'Calculatrice',
        'title_calculator_part2' => 'Taux de change',
        'title_exchange_part1' => 'Échangeur',
        'title_exchange_part2' => 'Taux de change',
        'title_converter_com1' => 'Taux de change',
        'title_converter_com2' => 'Convertisseur',
        'title_converter' => 'AMOUNT FROM_CODE à TO_CODE Convertisseur | Taux de conversion FROM_NAME - Changerio',
        'title_convert' => 'AMOUNT FROM_CODE à TO_CODE Conversion | Prix FROM_NAME dans TO_CODE - Changerio',
        'title_calculator' => 'AMOUNT FROM_CODE à TO_CODE Calculatrice | Taux de change FROM_NAME - Changerio',
        'title_exchange' => 'AMOUNT FROM_CODE à TO_CODE Exchanger | Taux de change FROM_NAME - Changerio',
        'title_converter_1' => 'Taux de change FROM_NAME (FROM_CODE) | Convertisseur FROM_NAME - Changerio',
        'title_converter_2' => 'Convertisseur FROM_CODE en TO_CODE | FROM_NAME - Changerio',
        'description_converter' => 'AMOUNT FROM_CODE à TO_CODE Taux de conversion. La conversion de AMOUNT FROM_NAME vers TO_NAME génère le taux de change de cette paire. Le convertisseur en ligne Changerio indique combien est AMOUNT FROM_NAME dans TO_NAME. Cryptocurrencies & Convertisseur de devises Fiat - Changerio',
        'description_convert' => 'AMOUNT FROM_CODE à TO_CODE Convertir les taux. La conversion de AMOUNT FROM_NAME vers TO_NAME génère le taux de change de cette paire. Le convertisseur en ligne Changerio indique combien est AMOUNT FROM_NAME dans TO_NAME. Cryptocurrencies & Convertisseur de devises Fiat - Changerio',
        'description_exchange' => 'AMOUNT FROM_CODE à TO_CODE Taux de change. L`échange entre AMOUNT FROM_NAME et TO_NAME génère le taux de change de cette paire. L`échangeur en ligne de Changerio indique combien est AMOUNT FROM_NAME dans TO_NAME. Cryptocurrencies & Fiat échange de devises - Changerio',
        'description_calculator' => 'AMOUNT FROM_CODE à TO_CODE Taux de change. Calculer de AMOUNT FROM_NAME à TO_NAME obtenir le taux de change de cette paire. La calculatrice en ligne Changerio indique combien est AMOUNT FROM_NAME dans TO_NAME. Calculateur de monnaie Cryptocurrencies & Fiat - Changerio',
        'og_locale' => 'en_US'
];