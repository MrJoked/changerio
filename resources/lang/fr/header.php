
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Header Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

        'currencies' => 'Monnaies',
        'language' => 'Langues',
        'search_box_text' => 'Rechercher une devise',
        'home' => 'Home',
        'cryptocurrencies' => 'Cryptomonnaies',
        'fiatcurrencies' => 'Monnaies fiduciaires',
        'ico' => 'ICO',
        'bc_convert' => 'Convertir',
        'bc_converter' => 'Convertisseur',
        'bc_calculator' => 'Calculatrice',
        'bc_exchange' => 'Échange'
  ];