<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
        'convert_button' => 'Convertisseur',
        'buysell_button' => 'Commerce',
        'exchange_button' => 'Échange',
        'active_button' => 'Actif',
        'upcoming_button' => 'Prochain',
        'previous_button' => 'Précédent'
];