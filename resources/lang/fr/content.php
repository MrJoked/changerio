<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Content Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    
        'home_main_title' => 'CONVERTISSEUR DE Cryptomonnaies',
        'home_sub_title' => 'Crypto à Fiat, Crypto à Crypto, Crypto à Crypto Convertisseur de devises',
        'home_button_convert' => 'Appuyez sur le bouton Convert',
        'home_convert_guide' => 'Taux Cryptomonnaies mis à jour toutes les 5 minutes et les taux de change Fiat mis à jour toutes les 1 heure.',
        'home_live_title' => 'Taux de conversion en direct',
        'home_recent_title' => 'Récemment converti',
        'home_crypto_title' => 'Cryptomonnaies',
        'home_fiat_title' => 'MONNAIES FIAT',
        'home_crypto_more' => 'Plus de Crypto-monnaies',
        'home_fiat_more' => 'Plus Fiat Devises',
        'converter_main_title' => 'CONVERTISSEUR DE Cryptomonnaies',
        'converter_sub_title' => 'Crypto à Fiat, Crypto à Crypto, Crypto à Crypto Convertisseur de devises',
        'converter_convert_guide' => 'Taux Cryptomonnaies mis à jour toutes les 5 minutes et les taux de change Fiat mis à jour toutes les 1 heure.',
        'converter_popular_title' => 'MONTANTS DE CONVERSION POPULAIRE',
        'converter_recent_title' => 'Récemment converti',
        'converter_fiat_currency' => 'DEVISE FIAT',
        'converter_crypto_currency' => 'Crypto-monnaie',
        'cryptocurrencies_title' => 'Liste des cryptomonnaies',
        'fiatcurrencies_title' => 'Liste de devises Fiat',
        'fiat_converter_title' => 'CONVERTISSEUR DE DEVISES',
        'ico_title' => 'Liste populaire ICO',
        'ico_name' => 'Prénom',
        'ico_description' => 'La description',
        'ico_start' => 'Date de début',
        'ico_end' => 'Date de fin',
        'ico_link' => 'Liens',
        'currency_title' => 'Les prix de la cryptomonnaie',
        'currency_title_btc' => 'Prix de crypto-monnaie en Bitcoin (BTC)',
        'currency_title_eth' => 'Prix de la cryptomonnaie à Ethereum (ETH)',
        'currency_title_usd' => 'Cryptomonnaie prix en Dollar américain (USD)',
        'currency_title_eur' => 'Cryptomonnaie prix en euro (EUR)',
        'currency_title_gbp' => 'Prix de cryptomonnaie en livre britannique (GBP)',
        'currency_title_inr' => 'Prix de cryptomonnaie en Roupie indienne (INR)',
        'currency_name' => 'Prénom',
        'currency_marketcap' => 'Marché',
        'currency_price' => 'Prix',
        'currency_volume' => 'Le volume',
        'currency_supply' => 'Monnaies maximales',
        'currency_change' => 'Changement',
        'common_hour' => 'Heure',
        'common_hours' => 'Heures',
        'common_minute' => 'Minute',
        'common_minutes' => 'Minutes',
        'common_to' => 'à',
        'common_ago' => 'il y a',
        'common_home' => 'Home',
        'title_converter' => 'Convertisseur',
        'subtitle_converter' => 'Taux de conversion',
        'title_convert' => 'Conversion',
        'subtitle_convert' => 'Taux de conversion',
        'title_calculator' => 'Calculatrice',
        'subtitle_calculator' => 'Calculatrice',
        'title_exchange' => 'Échange',
        'subtitle_exchange' => 'Taux d`échange',
        'title_converter_com' => 'Convertisseur',
        'subtitle_converter_com1' => 'Taux de conversion',
        'subtitle_converter_com2' => 'Taux d`échange',
        'terms_title' => 'Conditions d`utilisation',
        'privacy_title' => 'Politique de confidentialité',
        'contact_title' => 'Contactez nous',
        'disclaimer_title' => 'Avertissement',
        'disclaimer_text' => 'Aucun contenu ou information fourni dans notre newsletter ou sur ce site ne doit être considéré comme un investissement ou un conseil financier. Veuillez faire vos propres recherches avant de participer ou d`investir dans un ICO. Le contenu fourni ou lié sur ce site est à titre informatif seulement.',
        'price_chart' => 'Tableau des prix'
  ];
  