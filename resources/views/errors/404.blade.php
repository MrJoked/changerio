@extends('app')

@section('meta')
        <title>@lang('meta.home_title') | @lang('meta.company_name')</title>
        <meta itemprop="name" content="{{ trans('meta.home_title') }}" />
        <meta itemprop="description" content="{{ trans('meta.home_description') }}" />
        <meta name="description" content="{{ trans('meta.home_description') }}" />
        <meta name="keywords" content="{{ trans('meta.home_keywords') }}" />
        <meta name="twitter:title" content="{{ trans('meta.home_title').' | '.trans('meta.company_name') }}" />
        <meta name="twitter:description" content="{{ trans('meta.home_description') }}" />
        <meta name="og:title" content="{{ trans('meta.home_title').' | '.trans('meta.company_name') }}" />
        <meta name="og:description" content="{{ trans('meta.home_description') }}" />
        <meta name="og:url" content="https://changerio.com/" />
        <meta name="og:locale" content="{{ trans('meta.og_locale') }}" />

        <link rel="canonical" href="https://changerio.com/" />
        <link rel="alternate" href="https://changerio.com/" hreflang="x-default" />
        <link rel="alternate" hreflang="en" href="https://changerio.com/" />
        <link rel="alternate" hreflang="fr" href="https://changerio.com/fr/" />
<style type="text/css">
    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;    
    }
</style>
@endsection

@section('header')
    @include('layouts.header')
@endsection

@section('content')
        <div class="flex-center position-ref full-height" style="height: 60vh;">
            <div class="content">
                <div class="mt-5 mb-5 title text-center" style="height:100%">
                    <h1>Sorry, the page you are looking for could not be found. </h1>             
                </div>
            </div>
        </div>
@endsection
