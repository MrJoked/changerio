<div class="row conversion-block">
    <div class="owl-carousel">
            @for($i = 0; $i < 10; $i++)
                @if ($currencies)
                    @foreach ($currencies as $key => $rate)
                        <div class="item text-center padded">
                            <span class="title-c">{{ $key }}</span><br />
                            <span class="currency">{{ "$".(($rate->rate > 1)?round($rate->rate,1):round($rate->rate,4)) }}</span><br />
                            <span class="rating {{ ($rate->percent >= 0)?'change_up':'change_down' }}">{{ (($rate->percent>1)?round($rate->percent,1):round($rate->percent,2))."%" }}</span>
                        </div>
                    @endforeach
                @endif
            @endfor       
    </div>         
</div>
<script type="text/javascript">
    $(document).ready(function(){
        jQuery(".owl-carousel").owlCarousel({
            loop:true,
            slideBy:'page',
            margin:10,
            navSpeed: 300,
            autoplay:true,
            responsive:{
                0:{
                    items:4
                },
                600:{
                    items:5
                },
                1000:{
                    items:7
                }
            }
        });
    });
</script>