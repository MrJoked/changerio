<div className="popular">
        <div className="container">
          <Banner />
          <div className="row">
            <div className="col">
              <h3>{localize(content, 'converter_popular_title')}</h3>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-lg-6">
              <div className="block-currency">
                <ul className="first">
                  {popularAmounts.map(x => {
                    return (
                      <li key={Math.random() * 100}>
                        <img
                          src={firstImg}
                          alt={fromSt}
                          style="width: 25, height: 25, float: 'left'"/>
                        <Link onClick={() => this.handleRedirect(from.id, to.id, sum)}
                          to={`/converter/${lowerCase(fromSt)}/${lowerCase(toSt)}/${x}`}>{x} {fromSt} to {toSt}</Link>
                        <img
                          src={secondImg}
                          alt={toSt}
                          style="width: 25, height: 25, float: 'right'"/>
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
            <div className="col-12 col-lg-6">
              <div className="block-currency">
                <ul>
                  {popularAmounts.map(x => {
                    return (
                      <li key={Math.random() * 100}>
                        <img
                          src={firstImg}
                          alt={fromSt}
                          style="width: 25, height: 25, float: 'left'"/>
                        <Link onClick={() => this.handleRedirect(from.id, to.id, sum)}
                              to={`/converter/${lowerCase(fromSt)}/${lowerCase(toSt)}/${x}`}
                        >{x} {from.name} to {to.name}</Link>
                        <img
                          src={secondImg}
                          alt={toSt}
                          style="width: 25, height: 25, float: 'right'" />
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>