<footer>
    <div class="footer">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12 col-lg-4 col-xl-4 d-none d-sm-none  d-md-none d-lg-block text-left">
                    <a href="" class="footer-logo">
                        <img src="images/footer-logo.png" alt="">
                    </a>
                </div>
                <div class="col-12 col-md-12 col-lg-4 col-xl-4 text-center">
                    <ul>
                        <li>
                            <a href="{{ route('calculator') }}">@lang('footer.calculate')</a>
                        </li>
                        <li>
                            <a href="{{ route('convert') }}">@lang('footer.convert')</a>
                        </li>
                        <li>
                            <a href="{{ route('exchange') }}">@lang('footer.exchange')</a>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <a href="{{ route('terms') }}">@lang('footer.terms')</a>
                        </li>
                        <li>
                            <a href="{{ route('privacy') }}">@lang('footer.privacy')</a>
                        </li>
                        <li>
                            <a href="{{ route('contact') }}">@lang('footer.contact')</a>
                        </li>
                    </ul>
                    <p>© 2018 CryptoChanger. @lang('footer.arr').  </p>
                </div>
                <div class="col-12 col-md-12 col-lg-4 col-xl-4 text-right">
                    <div class="social">
                        <a href="">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-pinterest" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="{{ asset('/js/scripts.js') }}"></script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>