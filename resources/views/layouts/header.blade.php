    <div class="top-header">
        <div class="row">
            <div class="col-12 col-md-5 col-lg-6 col-xl-6 text-left">
                <span class="title">@lang('header.currencies'):</span>
                <a href="{{ route('currency','btc') }}">BTC</a>
                <a href="{{ route('currency','eth') }}">ETH</a>
                <a href="{{ route('currency','usd') }}">USD</a>
                <a href="{{ route('currency','eur') }}">EUR</a>
                <a href="{{ route('currency','gbp') }}">GBP</a>
                <a href="{{ route('currency','inr') }}">INR</a>
            </div>
            <div class="col-12 col-md-4 col-lg-4 col-xl-4 text-right">
                <span class="title">@lang('header.language'): </span>
                <div class="dropdown">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        @switch(App::getLocale())
                            @case('en')
                                 English
                            @break
                            @case('fr')
                                Français
                            @break
                            @default
                                English
                        @endswitch
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="/setlocale/en">English</a>
                        <a class="dropdown-item active" href="/setlocale/fr">Francais</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 col-lg-2 col-xl-2 input-header">
                <label for="search"></label>
                <input type="text" id="search" placeholder="{{ trans('header.search_box_text') }}">
                <div id="search_result">
                    <ul class="autosuggest">

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="{{ route('mainpage') }}">
                <img src="{{ asset('images/header-logo.png') }}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('cryptocurrencies') }}">@lang('header.cryptocurrencies')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('fiatcurrencies') }}">@lang('header.fiatcurrencies')</a>
                    </li>
                </ul>
            </div>
            {!! (isset($breadcrumbs))?$breadcrumbs:'' !!}
        </nav>
    </div>