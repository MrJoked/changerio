            <div class="convertation row slider-wrapp text-center">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <input id="sum" type="number" class="sum" placeholder="{{ (isset($sum))?$sum:'99' }}" value="{{ (isset($sum))?$sum:'99' }}">
                        </div>
                        @php
                            $from       = ((isset($from))?$from:'XRP');
                            $to         = ((isset($to))?$to:'USD');
                            $sum        = ((isset($sum))?$sum:'99');                        
                            $fromType   = ((isset($fromType))?$fromType:'c');
                            $toType     = ((isset($toType))?$toType:'f');                            
                            //lowercase
                            $froml = (isset($fromSelect) && !empty($fromSelect->currency_icon_name))?strtolower($fromSelect->currency_icon_name):'xrp';
                            $tol = (isset($toSelect) && !empty($toSelect->currency_icon_name))?strtolower($toSelect->currency_icon_name):'usd';
                            //currency name
                            $fromN = (isset($fromSelect))?$fromSelect->currency_icon_name:'ripple';
                            $toN = (isset($toSelect))?$toSelect->currency_icon_name:'usd';
                        @endphp
                        <div class="bottom-input mt-5 blockcenter">
                        <label for="inputState1">
                            <span>{{ (isset($ratiofromto))?'1'.$from.' = '.$ratiofromto.' '.$to:'' }}</span>
                            <div id="inputState1" class="">
                              <div class="dropdown" style="width: 100%;">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" style="border: 1px solid rgb(228, 229, 236); background-color: rgb(255, 255, 255); border-radius: 50px; width: 100%; max-width: 415px; height: 50px; text-align: center; color: rgb(155, 155, 155); font-weight: 400; font-family: Montserrat-SemiBold; overflow: hidden;">
                                  <img alt="" src="/images/{{ (($fromType=='f')?'fiat/'.$froml:'crypto/'.$fromN) }}.png" style="width: 25px; height: 25px; margin-right: 10px; margin-left: 10px; float: left;">{{ (isset($fromSelect))?$fromSelect->currency_key.' - '.$fromSelect->currency_name:'XRP - Ripple' }}
                                  <i class="fas fa-angle-down"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <input type="text" class="search_internal" placeholder="{{ trans('header.search_box_text') }}">
                                </div>
                              </div>
                              <div class="cont_list_select_mate"><ul class="cont_select_int"></ul>
                              </div></div>
                        </label>
                        <span class="virtualcurrency"></span>
                        <label for="inputState2">
                            <span>{{ (isset($ratiotofrom))?'1'.$to.' = '.$ratiotofrom.' '.$from:'' }}</span>
                             <div id="inputState2" class="">
                              <div class="dropdown" style="width: 100%;">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" style="border: 1px solid rgb(228, 229, 236); background-color: rgb(255, 255, 255); border-radius: 50px; width: 100%; max-width: 415px; height: 50px; text-align: center; color: rgb(155, 155, 155); font-weight: 400; font-family: Montserrat-SemiBold; overflow: hidden;">
                                  <img alt="" src="/images/{{ (($toType=='f')?'fiat/'.$tol:'crypto/'.$toN) }}.png" style="width: 25px; height: 25px; margin-right: 10px; margin-left: 10px; float: left;">{{ (isset($toSelect))?$toSelect->currency_key.' - '.$toSelect->currency_name:'USD - US Dollar' }}
                                  <i class="fas fa-angle-down"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <input type="text" class="search_internal2" placeholder="{{ trans('header.search_box_text') }}">             
                                </div>
                              </div>
                              <div class="cont_list_select_mate"><ul class="cont_select_int"></ul>
                              </div></div>
                        </label>
                        </div>
                        <div class="col-12 bottom-submit">
                            <input type="submit" class="button button-primary-1" value="{{ trans('buttons.convert_button') }}">
                        </div>
                        <div class="col-12">
                            <div class="result">
                                {{ (isset($result))?$sum.' '.$from.' = ':'' }} <span>{{ (isset($result))?$result:trans('content.home_button_convert') }}</span>{{ (isset($result))?' '.$to:'' }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="message">
                All figures are based on live mid-market rates. These rates are not available to consumer clients.
            </div>
<script type="text/javascript">
    $home = "{{ URL::to('/') }}";
    /********************************* Loading currencies at pageload ***************************************************/
    $( document ).ready(function() {
        $.ajaxSetup({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
       jQuery.ajax({
            url: "{{ url('/fiatcurrenciesadd') }}",
            method: 'post',
            data: {'data': 'add', 'num': 0, '_token': $('meta[name="csrf-token"]').attr('content')} ,
            success: function(result){    
                $count = Object.keys(result.success).length;
                for($i=0;$i<$count;$i++){
                    png = result['success'][$i]['currency_key'].toLowerCase();
                    $( "#inputState1 .dropdown-menu, #inputState2 .dropdown-menu" ).append('<a class="dropdown-item"><img alt="'+result['success'][$i]['currency_name']+'" src="/images/fiat/'+png+'.png" style="width: 20px; height: 20px; margin-right: 20px;">'+result['success'][$i]['currency_key']+' - '+result['success'][$i]['currency_name']+'</a>');
                    }
            }});
       jQuery.ajax({
            url: "{{ url('/cryptocurrenciesadd') }}",
            method: 'post',
            data: {'data': 'add', 'num': 0, '_token': $('meta[name="csrf-token"]').attr('content')} ,
            success: function(result){    
                $count = Object.keys(result.success).length;                
                for($i=0;$i<$count;$i++){
                    $( "#inputState1 .dropdown-menu, #inputState2 .dropdown-menu" ).append('<a class="dropdown-item"><img alt="'+result['success'][$i]['currency_name']+'" src="/images/crypto/'+result['success'][$i]['currency_icon_name']+'.png" style="width: 20px; height: 20px; margin-right: 20px;">'+result['success'][$i]['currency_key']+' - '+result['success'][$i]['currency_name']+'</a>');
                    }
                $return =  $( "#inputState1 .dropdown-menu" ).html();
                $return2 = $( "#inputState2 .dropdown-menu" ).html();
            }});
    });
var $fiatexisted = 100;
var $cryptoexisted = 100;
$( "#inputState1 .dropdown-menu, #inputState2 .dropdown-menu" ).on('resize scroll', function() {
            $cryptoexisted++;
            $fiatexisted++;            
            var $t=$(this); 
            if ($t.children().last().isInViewport()){ 
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
               jQuery.ajax({
                  url: "{{ url('/fiatcurrenciesadd') }}",
                  method: 'post',
                  data: {'data': 'add','num':$fiatexisted, '_token': $('meta[name="csrf-token"]').attr('content')} ,
                  success: function(result){
                    $count = Object.keys(result.success).length;                    
                    for($i=$fiatexisted;$i<$fiatexisted+$count;$i++){
                       $t.append('<a class="dropdown-item"><img alt="'+result['success'][$i]['currency_name']+'" src="/images/fiat/'+result['success'][$i]['currency_icon_name']+'.png" style="width: 20px; height: 20px; margin-right: 20px;">'+result['success'][$i]['currency_key']+' - '+result['success'][$i]['currency_name']+'</a>');
                    }
                    //$one = result['success'][0];
                    //console.log(result.success['0']);//+result['success'].length);
                  }});
                jQuery.ajax({
                  url: "{{ url('/cryptocurrenciesadd') }}",
                  method: 'post',
                  data: {'data': 'add','num':$cryptoexisted, '_token': $('meta[name="csrf-token"]').attr('content')} ,
                  success: function(result){
                    $count = Object.keys(result.success).length;                    
                    for($i=$cryptoexisted;$i<$cryptoexisted+$count;$i++){
                       $t.append('<a class="dropdown-item"><img alt="'+result['success'][$i]['currency_name']+'" src="/images/crypto/'+result['success'][$i]['currency_icon_name']+'.png" style="width: 20px; height: 20px; margin-right: 20px;">'+result['success'][$i]['currency_key']+' - '+result['success'][$i]['currency_name']+'</a>');
                    }
                    //$one = result['success'][0];
                     //console.log(result.success['0']);//+result['success'].length);
                  }});
            
            } 
        });

</script>