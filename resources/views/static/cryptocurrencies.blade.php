@extends('app')

@section('meta')
		  <title>@lang('meta.cryptocurrencies_title') | @lang('meta.company_name') </title>
          <meta itemprop="name" content="{{ trans('meta.cryptocurrencies_title') }}" />
          <meta itemprop="description" content="{{ trans('meta.cryptocurrencies_description') }}" />
          <meta name="description" content="{{ trans('meta.cryptocurrencies_description') }}" />
          <meta name="twitter:title" content="{{ trans('meta.cryptocurrencies_title').' | '.trans('meta.company_name') }}" />
          <meta name="twitter:description" content="{{ trans('meta.cryptocurrencies_description') }}" />
          <meta name="og:title" content="{{ trans('meta.cryptocurrencies_title').' | '.trans('meta.company_name') }}" />
          <meta name="og:description" content="{{ trans('meta.cryptocurrencies_description') }}" />
          <meta name="og:url" content="https://changerio.com/cryptocurrencies" />
          <meta name="og:locale" content="{{ trans('meta.og_locale') }}" />

          <link rel="canonical" href="https://changerio.com/cryptocurrencies" />
          <link rel="alternate" href="https://changerio.com/cryptocurrencies" hreflang="x-default" />
          <link rel="alternate" hreflang="fr" href="https://changerio.com/fr/cryptocurrencies" />
          <link rel="alternate" hreflang="en" href="https://changerio.com/cryptocurrencies" />
@endsection


@section('header')
	@include('layouts.header')
@endsection

@section('mainclass', 'page')

@section('content')
 <div class="container">
        <div class="blocks text-center">
            @include('layouts.banner', ['position' => 'cryptocurrencies_top'])
        </div>
        <div class="row">
            <div class="col-12">
                <h3>{{ trans('content.cryptocurrencies_title') }}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12">
                <ul class="list-c-two">
                    @foreach ($currencies as $currency)
                        <li class="currency">
                            <img src="{{asset('images/crypto/'.strtolower($currency->currency_name).'.png') }}" alt="">
                            <div class="title">
                                <a href="">{{ $currency->currency_key.' - '.$currency->currency_name }} </a>
                            </div>
                            <div class="" style="float:right;">
                                <a href="{{ url('/converter/'.strtolower($currency->currency_key).'/usd/99') }}" class="button button-primary-1">@lang('buttons.convert_button')</a>
                                <a href="{{ $currency->currency_buysellurl }}" class="button button-primary-1">@lang('buttons.buysell_button')</a>
                                <a href="{{ $currency->currency_exchangeurl }}" class="button button-primary-1">@lang('buttons.exchange_button')</a>   
                            </div>                         
                        </li>
                    @endforeach   
                </ul>
            </div>
        </div>
        <div class="blocks bannercrypto_bottom text-center">
            @include('layouts.banner', ['position' => 'cryptocurrencies_bottom'])
        </div>
    </div>
    <script type="text/javascript">
$convert_button = "@lang('buttons.convert_button')";

$buysell_button = "@lang('buttons.buysell_button')";
$buysell_button_url = "@lang('buttons.currency_buysellurl')";

$exchange_button = "@lang('buttons.exchange_button')";
$exchange_button_url = "@lang('buttons.currency_exchangeurl')";

$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};


        $(window).on('resize scroll', function() {
          $existed = $('.currency').length;
            if ($('.bannercrypto_bottom').isInViewport()) {
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ url('/cryptocurrenciesadd') }}",
                  method: 'post',
                  data: {'data': 'add','num':$('li.currency').length, '_token': $('meta[name="csrf-token"]').attr('content')} ,
                  success: function(result){
                    $count = Object.keys(result.success).length;                    
                    for($i=$existed;$i<$existed+$count;$i++){
                       $( ".list-c-two" ).append('<li class="currency"><img src="images/crypto/'+result['success'][$i]['currency_name'].toLowercase()+'.png" alt=""><div class="title"><a href="">'+result.success[$i]['currency_key']+' - '+result.success[$i]['currency_name']+'</a></div><div class="" style="float:right;"><a href="" class="button button-primary-1">'+$convert_button+'</a><a href="'+$buysell_button_url+'" class="button button-primary-1">'+$buysell_button+'</a><a href="'+$exchange_button_url+'" class="button button-primary-1">'+$exchange_button+'</a></div></li>');
                    }
                    //$one = result['success'][0];
                     console.log(result.success['0']);//+result['success'].length);
                  }});
            
            } 
        });

</script>
@endsection

