@extends('app')

@section('meta')
		<title>@lang('meta.home_title') | @lang('meta.company_name')</title>
        <meta itemprop="name" content="{{ trans('meta.home_title') }}" />
        <meta itemprop="description" content="{{ trans('meta.home_description') }}" />
        <meta name="description" content="{{ trans('meta.home_description') }}" />
        <meta name="keywords" content="{{ trans('meta.home_keywords') }}" />
        <meta name="twitter:title" content="{{ trans('meta.home_title').' | '.trans('meta.company_name') }}" />
        <meta name="twitter:description" content="{{ trans('meta.home_description') }}" />
        <meta name="og:title" content="{{ trans('meta.home_title').' | '.trans('meta.company_name') }}" />
        <meta name="og:description" content="{{ trans('meta.home_description') }}" />
        <meta name="og:url" content="https://changerio.com/" />
        <meta name="og:locale" content="{{ trans('meta.og_locale') }}" />

        <link rel="canonical" href="https://changerio.com/" />
        <link rel="alternate" href="https://changerio.com/" hreflang="x-default" />
        <link rel="alternate" hreflang="en" href="https://changerio.com/" />
        <link rel="alternate" hreflang="fr" href="https://changerio.com/fr/" />

        <link rel="stylesheet" href="{{ asset('/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/owl.theme.default.min.css') }}">
@endsection

@section('header')
	@include('layouts.header')
   <div class="header-slider">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h1>@lang('content.home_main_title')</h1>
                    <span class="sub">@lang('content.home_sub_title')</span>
                </div>
            </div>
          @include('layouts.convertation_block')
        </div>
   </div>
@endsection

@section('content')
<div class="live-block">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2> @lang('content.home_live_title') </h2>
                </div>
            </div>
                @include('layouts.livestream', ['currencies' => $liveStream])
        </div>
    </div>
    <div class="recent-convert">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>@lang('content.home_recent_title')</h3>
                </div>
            </div>
            <div class="row recent-block">
                <div class="col-12 col-lg-6 wrap">
                    <ul>
                        @foreach($leftRecent as $convert)
                        <li>
                            <img src="{{ asset('/images/'.$convert['fromType'].'/'.strtolower($convert['fromurl']).'.png') }}">
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <img src="{{ asset('/images/'.$convert['toType'].'/'.strtolower($convert['tourl']).'.png') }}">
                            <span><a href="{{ '/converter/'.strtolower($convert['from']).'/'.strtolower($convert['to']).'/'.$convert['sum'] }}">{{ $convert['sum'].' '.$convert['from'].' to '.$convert['to'] }}</a></span>
                            <span>{{ (($convert['Interval']->h>0)?$convert['Interval']->h.' hour ':'').$convert['Interval']->i.' ' }} minutes ago</span>
                        </li> 
                        @endforeach                     
                    </ul>
                </div>
                <div class="col-12 col-lg-6 wrap">
                    <ul>
                        @foreach($rightRecent as $convert)
                        <li>
                            <img src="{{ asset('/images/'.$convert['fromType'].'/'.strtolower($convert['fromurl']).'.png') }}">
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <img src="{{ asset('/images/'.$convert['toType'].'/'.strtolower($convert['tourl']).'.png') }}">
                            <span><a href="{{ '/converter/'.strtolower($convert['from']).'/'.strtolower($convert['to']).'/'.$convert['sum'] }}">{{ $convert['sum'].' '.$convert['from'].' to '.$convert['to'] }}</a></span>
                            <span>{{ (($convert['Interval']->h>0)?$convert['Interval']->h.' hour ':'').$convert['Interval']->i.' ' }} minutes ago</span>
                        </li> 
                        @endforeach                          
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="currencies-block">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <div class="title"> @lang('content.home_crypto_title') </div>
                    <ul>
                        @foreach($crypto as $currency)
                        <li>
                            <a href="{{ url('/converter/'.$currency->currency_key) }}">
                                <img src="{{ asset('/images/crypto/'.strtolower($currency->currency_name).'.png') }}" ></span>
                                <span>{{ $currency->currency_key.' - '.$currency->currency_name}}</span>
                            </a>
                        </li>
                        @endforeach
                        <li>
                            <a href="{{ route('cryptocurrencies') }}">                                
                                <span class="link">@lang('content.home_crypto_more')</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-md-6">
                    <div class="title">@lang('content.home_fiat_title') </div>
                    <ul>
                        @foreach($fiat as $currency)
                        <li>
                            <a href="{{ url('/converter/'.$currency->currency_key) }}">
                                <img src="{{ asset('/images/fiat/'.strtolower($currency->currency_key).'.png') }}" ></span>
                                <span>{{ $currency->currency_key.' - '.$currency->currency_name}}</span>
                            </a>
                        </li>
                        @endforeach                        
                        <li>
                            <a href="{{ route('fiatcurrencies') }}">
                                <span class="link">@lang('content.home_fiat_more')</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="blocks text-center">
                @include('layouts.banner', ['position' => 'mainpage_bottom'])
            </div>
        </div>
    </div>
@endsection
