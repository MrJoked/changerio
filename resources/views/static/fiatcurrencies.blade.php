@extends('app')

@section('meta')
		  <title>@lang('meta.fiatcurrencies_title') | @lang('meta.company_name') </title>
          <meta itemprop="name" content="{{ trans('meta.fiatcurrencies_title') }}" />
          <meta name="description" content="{{ trans('meta.fiatcurrencies_description') }}" />
          <meta name="twitter:title" content="{{ trans('meta.fiatcurrencies_title').' | '.trans('meta.company_name') }}" />
          <meta name="twitter:description" content="{{ trans('meta.home_description') }}" />
          <meta name="og:title" content="{{ trans('meta.fiatcurrencies_title').' | '.trans('meta.company_name') }}" />
          <meta name="og:description" content="{{ trans('meta.fiatcurrencies_description') }}" />
          <meta name="og:url" content="https://changerio.com/fiatcurrencies" />
          <meta name="og:locale" content="{{ trans('meta.og_locale') }}" />

          <link rel="canonical" href="https://changerio.com/fiatcurrencies" />
          <link rel="alternate" href="https://changerio.com/fiatcurrencies" hreflang="x-default" />
          <link rel="alternate" hreflang="fr" href="https://changerio.com/fr/fiatcurrencies" />
          <link rel="alternate" hreflang="en" href="https://changerio.com/fiatcurrenciessss" />
@endsection

@section('header')
	@include('layouts.header')
@endsection

@section('mainclass', 'page')

@section('content')
 <div class="container">
        <div class="blocks text-center">
            @include('layouts.banner', ['position' => 'fiatcurrencies_top'])
        </div>
        <div class="row">
            <div class="col-12">
                <h3>{{ trans('content.fiatcurrencies_title') }} </h3>
            </div>
        </div>
        <div class="row">
            <div class="first col-12 col-lg-8">
                <ul class="list-c">
                    @foreach ($currencies as $currency)
                        <li class="currency">
                            <img src="{{asset('images/fiat/'.strtolower($currency->currency_key).'.png') }}" alt="">
                            <div class="title"><a href="">{{ $currency->currency_key.' - '.$currency->currency_name }}</a></div>
                            <a href="{{ url('/converter/'.strtolower($currency->currency_key) ) }}" class="button button-primary-1">@lang('buttons.convert_button')</a>
                        </li>
                    @endforeach                   
                </ul>
            </div>
            <div class="last col-12 col-lg-4">
                <div class="calculator">
                    <div class="title">@lang('content.fiat_converter_title')</div>
                    <input type="number" class="sum" placeholder="100000.50" value="100000.50">
                    <div class="bottom-input">
                        <label for="inputState1">
                            <div id="inputState1" class="" data-mate-select="active">
                              <div class="dropdown" style="width: 100%;">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" style="border: 1px solid rgb(228, 229, 236); background-color: rgb(255, 255, 255); border-radius: 50px; width: 100%; max-width: 415px; height: 50px; text-align: center; color: rgb(155, 155, 155); font-weight: 400; font-family: Montserrat-SemiBold; overflow: hidden;">
                                  <img alt="" src="/images/fiat/usd.png" style="width: 25px; height: 25px; margin-right: 10px; margin-left: 10px; float: left;">USD - US dollars
                                  <i class="fas fa-angle-down"></i>
                                </button>
                                <div class="dropdown-menu">
                          
                                </div>
                              </div>
                              <div class="cont_list_select_mate"><ul class="cont_select_int"></ul>
                              </div></div>
                        </label>
                        <span class="virtualcurrency"></span>
                        <label for="inputState2">
                             <div id="inputState2" class="" data-mate-select="active">
                              <div class="dropdown" style="width: 100%;">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" style="border: 1px solid rgb(228, 229, 236); background-color: rgb(255, 255, 255); border-radius: 50px; width: 100%; max-width: 415px; height: 50px; text-align: center; color: rgb(155, 155, 155); font-weight: 400; font-family: Montserrat-SemiBold; overflow: hidden;">
                                  <img alt="" src="/images/fiat/eur.png" style="width: 25px; height: 25px; margin-right: 10px; margin-left: 10px; float: left;">EUR - Euros
                                  <i class="fas fa-angle-down"></i>
                                </button>
                                <div class="dropdown-menu">
                                    
                                </div>
                              </div>
                              <div class="cont_list_select_mate"><ul class="cont_select_int"></ul>
                              </div></div>
                        </label>
                    </div>
                    <input type="submit" class="button button-primary-1" value="{{ trans('buttons.convert_button') }}">
                </div>
                <div class="d-none d-lg-block blocks-sidebar text-center">
                    @include('layouts.banner', ['position' => 'fiatcurrencies_sidebar'])
                </div>
            </div>
        </div>
        <div class="bannerfiat_bottom text-center">
            @include('layouts.banner', ['position' => 'fiatcurrencies_bottom'])
        </div>
    </div>
<script type="text/javascript">
$button = "@lang('buttons.convert_button')";
        /******* Loading all currencies into selects *******/
        $('#inputState1 .btn, #inputState2 .btn').click(function(){
            $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
               jQuery.ajax({
                  url: "{{ url('/fiatcurrenciesadd') }}",
                  method: 'post',
                  data: {'data': 'add', 'num': 'all', '_token': $('meta[name="csrf-token"]').attr('content')} ,
                  success: function(result){    
                    $count = Object.keys(result.success).length;                
                    for($i=0;$i<$count;$i++){
                       $( "#inputState1 .dropdown-menu, #inputState2 .dropdown-menu" ).append('<a class="dropdown-item"><img alt="'+result['success'][$i]['currency_name']+'" src="/images/fiat/'+result['success'][$i]['currency_key'].toLowerCase()+'.png" style="width: 20px; height: 20px; margin-right: 20px;">'+result['success'][$i]['currency_key']+' - '+result['success'][$i]['currency_name']+'</a>');
                    }
                  }});
        });

        /***** Loading next 100 currencies ******/
        $(window).on('resize scroll', function() {
          $existed = $('.currency').length;
            if ($('.bannerfiat_bottom').isInViewport()) {
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
               jQuery.ajax({
                  url: "{{ url('/fiatcurrenciesadd') }}",
                  method: 'post',
                  data: {'data': 'add','num':$('li.currency').length, '_token': $('meta[name="csrf-token"]').attr('content')} ,
                  success: function(result){
                    $count = Object.keys(result.success).length;                    
                    for($i=$existed;$i<$existed+$count;$i++){
                       $( ".list-c" ).append('<li class="currency"><img src="images/fiat/'+result['success'][$i]['currency_icon_name'].toLowerCase()+'.png" alt=""><div class="title"><a href="">'+result.success[$i]['currency_key']+'</a></div><a href="" class="button button-primary-1">'+$button+'</a></li>');
                    }
                    //$one = result['success'][0];
                     console.log(result.success['0']);//+result['success'].length);
                  }});
            
            } 
        });

</script>
@endsection