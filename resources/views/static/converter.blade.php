@extends('app')

@section('meta')
          <title>{{ $sum.' '.$from.' to '.$to.' Converter |'.trans('meta.home_title') }}</title>
          <meta itemprop="name" content="{{ $sum.' '.$from.' to '.$to }} Exchange Rate - Changerio"/>
          <meta name="twitter:title" content="{{ $sum.' '.$from.' to '.$to }} Exchange Rate - Changerio"/>
@endsection


@section('header')
  @include('layouts.header')
  @php
      $from       = ((isset($from))?$from:'XRP');
      $to         = ((isset($to))?$to:'USD');
  @endphp
   <div class="header-slider">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h1>{{ $from.' TO '.$to.' '.$routename }}</h1>
                    <span class="sub">{{ $fromSelect->currency_name.' to '.$toSelect->currency_name.' '.$subtitle }}</span>
                </div>
            </div>
          @include('layouts.convertation_block')
        </div>
   </div>
@endsection

@section('mainclass', 'page')

@section('content')
         @if(isset($rates) && $rates !='')
          <div class="chart">
            <div class="container">
              <div class="row">
                  <div class="col-12">
                      <h2>{{ $from.' / '.$to.' PRICE CHART'}}</h2>
                  </div>
              </div>
            </div>
            <div class="container">
              <div class="row">

                <div class="col-12">                  
                    <div id="chart_rates"></div>
                       

                    <?= Lava::render('ColumnChart', 'Rates', 'chart_rates'); ?>
                      
                </div>
              </div>
            </div>
          </div>
        @endif 

     
        <div class="popular">
        <div class="container">
          <div class="blocks text-center">
                  @include('layouts.banner', ['position' => 'converter_top'])
          </div>
          <div class="row">
            <div class="col">
              <h3>@lang('content.converter_popular_title')</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-lg-6">
              <div class="block-currency">
                <ul class="first">
                  @foreach($popsum as $pair)
                    <li>
                      <img src="/images/{{ (($fromType == 'c')?'crypto':'fiat').'/'.strtolower(($fromType == 'c')?$fromSelect->currency_icon_name:$fromSelect->currency_code) }}.png" alt="{{ $popfrom[0]->currency_key }}" style="width: 25px; height: 25px; float: left;">
                      <a href="/converter/{{ strtolower($popfrom[0]->currency_key).'/'.strtolower($popto[0]->currency_key).'/'.$pair }}">{{ $pair.' '.$popfrom[0]->currency_key .' to '.$popto[0]->currency_key }} </a>
                      <img src="/images/{{ (($toType == 'c')?'crypto':'fiat').'/'.strtolower($popto[0]->currency_key) }}.png" alt="{{ $popto[0]->currency_key }}" style="width: 25px; height: 25px; float: right;">
                    </li>
                  @endforeach
                </ul>
              </div>
            </div>
            <div class="col-12 col-lg-6">
              <div class="block-currency">
                <ul>
                  @foreach($popsum as $pair)
                    <li>
                      <img src="/images/{{ (($fromType == 'f')?'fiat':'crypto').'/'.strtolower(($fromType == 'c')?$fromSelect->currency_icon_name:$fromSelect->currency_code) }}.png" alt="{{ $popfrom[0]->currency_key }}" style="width: 25px; height: 25px; float: left;">
                      <a href="/converter/{{ strtolower($popfrom[0]->currency_key).'/'.strtolower($popto[0]->currency_key).'/'.$pair }}">{{ $pair.' '.$popfrom[0]->currency_name .' to '.$popto[0]->currency_name }} </a>
                      <img src="/images/{{ (($toType == 'f')?'fiat':'crypto').'/'.strtolower($popto[0]->currency_key) }}.png" alt="{{ $popto[0]->currency_key }}" style="width: 25px; height: 25px; float: right;">
                    </li>
                  @endforeach                 
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

        <div class="convert-c">
          <div class="container">
            <div class="row">
              <div class="col-12 col-lg-5 col-xl-4"><h3>@lang('content.converter_recent_title')</h3>
                  <div class="block-c">
                    <ul>
                      @foreach($recent as $convert)
                      <li>
                        <img src="{{ asset('/images/'.$convert['fromType'].'/'.strtolower($convert['fromurl']).'.png') }}" alt="{{ $convert['from'] }}" style="width: 25px; height: 25px;">
                          <span>
                            <a href="{{ '/converter/'.strtolower($convert['from']).'/'.strtolower($convert['to']).'/'.$convert['sum'] }}">{{ $convert['sum'].' '.$convert['from'].' to '.$convert['to'] }}</a>
                          </span><span>{{ (($convert['Interval']->h>0)?$convert['Interval']->h.' hour ':'').$convert['Interval']->i.' ' }} minutes ago</span>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                </div>
              <div class="col-12 col-lg-7 col-xl-8">
                <div class="row">
                  <div class="col-12 col-lg-6">
                      <div><h3>{{ $from }} TO FIAT CURRENCY </h3>
                        <div class="block-currency">
                          <ul>
                            @foreach($btctofiat as $currency)
                            <li>
                              <img class="currency-table-img currency-table-img-left" src="{{ asset('/images/'.(($fromType == 'f')?'fiat':'crypto').'/'.strtolower(($fromType == 'c')?$fromSelect->currency_icon_name:$fromSelect->currency_code).'.png') }}" alt="{{ $from }}">
                                <a href="{{ url('/converter/'.strtolower($from).'/'.strtolower($currency->currency_key).'/90') }}">{{ $from.' to '.$currency->currency_key }}</a>
                              <img class="currency-table-img currency-table-img-right" src="{{ asset('/images/fiat/'.strtolower($currency->currency_key).'.png') }}" alt="{{ $currency->currency_key }}">
                            </li>
                            @endforeach
                          </ul>
                        </div>
                      </div>
                  </div>
                                    <div class="col-12 col-lg-6">
                    <div><h3>{{ $from }} TO CRYPTOCURRENCY </h3>
                        <div class="block-currency">
                          <ul>
                            @foreach($btctocrypto as $currency)
                            <li>
                              <img class="currency-table-img currency-table-img-left" src="{{ asset('/images/'.(($fromType == 'f')?'fiat':'crypto').'/'.strtolower(($fromType == 'c')?$fromSelect->currency_icon_name:$fromSelect->currency_code).'.png') }}" alt="{{ $from }}">
                                <a href="{{ url('/converter/'.strtolower($from).'/'.strtolower($currency->currency_key).'/90') }}">{{ $from.' to '.$currency->currency_key }}</a>
                              <img class="currency-table-img currency-table-img-right" src="{{ asset('/images/crypto/'.strtolower($currency->currency_name).'.png') }}" alt="{{ $currency->currency_key }}">
                            </li>
                            @endforeach
                          </ul>
                        </div>
                      </div>
                    </div>
                  <div class="col-12 col-lg-6">
                    <div><h3>FIAT CURRENCY TO {{ $from }} </h3>
                        <div class="block-currency">
                          <ul>
                            @foreach($fiattobtc as $currency)
                            <li>
                             <img class="currency-table-img currency-table-img-left" src="{{ asset('/images/fiat/'.strtolower($currency->currency_key).'.png') }}" alt="{{ $currency->currency_key }}"> 
                                <a href="{{ url('/converter/btc/'.strtolower($currency->currency_key).'/90') }}">{{ $currency->currency_key.' to '.$from }}</a>                              
                              <img class="currency-table-img currency-table-img-right" src="{{ asset('/images/'.(($fromType == 'f')?'fiat':'crypto').'/'.strtolower(($fromType == 'c')?$fromSelect->currency_icon_name:$fromSelect->currency_code).'.png') }}" alt="{{ $from }}">
                            </li>
                            @endforeach
                          </ul>
                        </div>
                      </div>
                  </div>
                  <div class="col-12 col-lg-6">
                    <div><h3>CRYPTOCURRENCY CURRENCY TO {{ $from }} </h3>
                        <div class="block-currency">
                          <ul>
                            @foreach($cryptotobtc as $currency)
                            <li>
                               <img class="currency-table-img currency-table-img-left" src="{{ asset('/images/crypto/'.strtolower($currency->currency_name).'.png') }}" alt="{{ $currency->currency_key }}">
                                <a href="{{ url('/converter/btc/'.strtolower($currency->currency_key).'/90') }}">{{ $currency->currency_key.' to '.$from }}</a>                             
                              <img class="currency-table-img currency-table-img-right" src="{{ asset('/images/'.(($fromType == 'f')?'fiat':'crypto').'/'.strtolower(($fromType == 'c')?$fromSelect->currency_icon_name:$fromSelect->currency_code).'.png') }}" alt="{{ $from }}">
                            </li>
                            @endforeach
                          </ul>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="blocks text-center">
             @include('layouts.banner', ['position' => 'converter_bottom'])
            </div>
          </div>
        </div>
@endsection