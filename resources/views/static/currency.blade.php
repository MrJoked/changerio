@extends('app')

@section('meta')
          <title>{{ $title }} | @lang('meta.company_name') </title>
          <meta itemprop="name" content="{{ $title }}" />
          <meta itemprop="description" content="@lang('meta.currency_description')" />
          <meta name="description" content="@lang('meta.currency_description')" />
          <meta name="twitter:title" content="{{ $title }} | @lang('meta.company_name')" />
          <meta name="twitter:description" content="@lang('meta.currency_description')" />
          <meta name="og:title" content="{{ $title }} | @lang('meta.company_name')" />
          <meta name="og:description" content="@lang('meta.currency_description')" />
          <meta name="og:url" content="https://changerio.com/currency" />
          <meta name="og:locale" content="@lang('meta.og_locale')" />

          <link rel="canonical" href="https://changerio.com/currency/{{ $id }}" />
          <link rel="alternate" href="https://changerio.com/currency/{{ $id }}" />
          <link rel="alternate" hreflang="en" href="https://changerio.com/currency/{{ $id }}" />
          <link rel="alternate" hreflang="fr" href="https://changerio.com/currency/{{ $id }}" />
@endsection


@section('header')
	@include('layouts.header')
@endsection

@section('mainclass', 'page')

@section('content')
 <div class="container">
        <div class="blocks text-center">
            @include('layouts.banner', ['position' => 'iac_top'])
        </div>
        <div class="row">
            <div class="col-12">
                <h3>{{ $title }}</h3>
            </div>
        </div>
        <div class="row header-block-btc">
          <div class="col-12 col-lg-3 col-xl-3">@lang('content.currency_name')</div>
          <div class="col-12 col-lg-2 col-xl-2">@lang('content.currency_price')</div>
          <div class="col col-sm-2">@lang('content.currency_marketcap')</div>
          <div class="col col-sm">@lang('content.currency_volume')</div>
          <div class="col col-sm">@lang('content.currency_supply')</div>
          <div class="col col-sm-1 text-center">@lang('content.currency_change')</div>
        </div>
        <div class="list-c">
        @foreach ($currencies as $currency)
                    @php
                        $lowsymbol = strtolower($id);
                        $lowcurrent = strtolower($currency['symbol']);
                        $marketcup = $currency['market_cap_'.$lowsymbol];
                        $volume = $currency['24h_volume_'.$lowsymbol];
                        $price = '--';
                        if($currency['price_'.$lowsymbol] > 1){ 
                            $price = round($currency['price_'.$lowsymbol],2); 
                        }elseif($currency['price_'.$lowsymbol] < 1){   
                            $price = round($currency['price_'.$lowsymbol],8);  
                        }elseif(($lowcurrent == 'btc' && $lowcurrent == 'btc') || ($lowcurrent == 'eth' && $lowcurrent == 'eth')) {
                             $price = '1.00000000';
                        }
                    @endphp

             <div class="row align-items-center body-block-btc currency-page-list">
                 <div class="col-12 col-lg-3 col-xl-3">
                    <img src="{{ asset('/images/crypto/'.$currency['id']).'.png' }}" alt="{{ $currency['id'] }}"/>
                    <span class="name">
                        <a href="/converter/{{ $lowcurrent }}">{{ $currency['name']."(".$currency['symbol'].")" }}</a>
                    </span>
                </div>

                <div class="col-12 col-lg-2 col-xl-2">
                    <span class="price" title="{{ $currency['price_'.$lowsymbol] }}">
                        <a href="/convert/{{ $lowcurrent.'/'.$lowsymbol.'/'.$price  }}">
                            {{ $symbol.$price  }}</a>
                    </span>
                </div>
                <div class="col-12 col-sm-6 col-md-2">
                    <span class="description" title="{{ $currency['market_cap_'.$lowsymbol] }}">
                        {{ $symbol.((isset($marketcup))?round($marketcup,2):'--') }} 
                    </span>
                </div>
                <div class="col-12 col-sm-6 col-md">
                    <span class="ending" title="{{ $volume }}">
                        {{ $symbol.((isset($volume))?round($volume,2):'--')  }} 
                    </span>
                </div>
                <div class="col-12 col-sm-6 col-md">
                    <span class="starting" title="maxSupply">
                        {{ ($currency['max_supply'])?round($currency['max_supply'],2):"---"  }} 
                    </span>
                </div>
                <div class="col-12 col-sm-6 col-md-1">
                    <span class="@if ($currency['percent_change_24h'] > 0)
                                        change_up
                                 @elseif($currency['percent_change_24h'] < 0)
                                        change_down
                                 @endif">
                        {{ $currency['percent_change_24h'] }}
                    </span>
                </div>
            </div>
        @endforeach         
        </div>
        <div class="loading">Loading...</div>
        <div class="blocks banneriac_bottom text-center">
            @include('layouts.banner', ['position' => 'iac_bottom'])
        </div>
    </div>
<script type="text/javascript"> 
$id = "{{ $id }}";
$symbol = "{{ $symbol }}";
$lowsymbol = "{{ $lowsymbol }}";
$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};


        $(window).on('resize scroll', function() {
          $existed = $('.body-block-btc').length;
            if ($('.banneriac_bottom').isInViewport()) {
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
               $('.loading').show(300);
               jQuery.ajax({
                  url: "{{ url('/currency/'.$lowsymbol) }}",
                  method: 'post',
                  data: {'data': 'add','num':$existed, '_token': $('meta[name="csrf-token"]').attr('content')} ,
                  success: function($result){                    
                    $count = Object.keys($result.success).length;     
                    $('.loading').hide(300);               
                    for($i=$existed;$i<$existed+$count;$i++){                        
                        //$lowsymbol = $result['success'][$i]['$id'].toLowerCase();
                        if(typeof($result['success'][$i]) !='undefined'){
                            $lowcurrent = $result['success'][$i]['symbol'].toLowerCase();
                            $price_raw = Number($result['success'][$i]['price_'+$lowsymbol]);
                            $price = ($price_raw  > 1)? $price_raw.toFixed(2): $price_raw.toFixed(8);
                            $marketcup = Number($result['success'][$i]['market_cap_'+$lowsymbol]).toFixed(2);
                            $marketcup = (typeof($marketcup) != "undefined")?$marketcup:"--";
                            $volume = Number($result['success'][$i]['24h_volume_'+$lowsymbol]).toFixed(2);
                            $volume = (typeof($volume) != "undefined")?$volume:"--";
                            $change_24h = $result['success'][$i]['percent_change_24h'];
                            $max_supply = Number($result['success'][$i]['max_supply']).toFixed(2);

                            $( ".list-c" ).append('<div class="row align-items-center body-block-btc currency-page-list" key={uniqueId(currency.symbol)}><div class="col-12 col-lg-3 col-xl-3"><img src="/images/crypto/'+$result['success'][$i]['id']+'.png" alt="'+$result['success'][$i]['id']+'"/><span class="name"><a href="/converter/'+$lowsymbol+'">'+$result['success'][$i]['name']+'('+$result['success'][$i]['symbol']+')</a></span></div>');

                            $(".body-block-btc").last().append('<div class="col-12 col-lg-2 col-xl-2"><span class="price" title="'+$result['success'][$i]['price_'+$lowsymbol]+'"><a href="/convert/'+$lowcurrent+'/'+$lowsymbol+'/'+$price+'">'+$symbol+$price+'</a></span></div>');
                       
                            $(".body-block-btc").last().append('<div class="col-12 col-sm-6 col-md-2"><span class="description" title="'+$marketcup+'">'+$symbol+$marketcup+'</span></div>');

                            $(".body-block-btc").last().append('<div class="col-12 col-sm-6 col-md"><span class="ending" title="'+$volume+'">'+$symbol+$volume+'</span></div>');

                            $(".body-block-btc").last().append('<div class="col-12 col-sm-6 col-md"><span class="starting" title="maxSupply">'+$max_supply+'</span></div>');

                            $(".body-block-btc").last().append('<div class="col-12 col-sm-6 col-md-1"><span class="'+(($change_24h>0)?'change_up':'change_down')+'">'+$change_24h+'</span></div>');

                            $( ".list-c" ).append('</div>');
                        }
                    }
                  }});
            
            } 
        });
</script>
@endsection

