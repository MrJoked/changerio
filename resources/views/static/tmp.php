                  <div class="col-12 col-lg-6">
                    <div><h3>BTC TO CRYPTOCURRENCY </h3>
                        <div class="block-currency">
                          <ul>
                            @foreach($btctocrypto as $currency)
                            <li>
                              <img class="currency-table-img currency-table-img-left" src="{{ asset('/images/crypto/bitcoin.png') }}" alt="BTC">
                                <a href="{{ url('/converter/btc/'.strtolower($currency->currency_key).'/90') }}">BTC to {{ $currency->currency_key }}</a>
                              <img class="currency-table-img currency-table-img-right" src="{{ asset('/images/fiat/'.strtolower($currency->currency_key).'.png') }}" alt="{{ $currency->currency_key }}">
                            </li>
                            @endforeach
                          </ul>
                        </div>
                      </div>
                    </div>
                  <div class="col-12 col-lg-6">
                    <div><h3>FIAT CURRENCY TO BTC </h3>
                        <div class="block-currency">
                          <ul>
                            @foreach($fiattobtc as $currency)
                            <li>
                              <img class="currency-table-img currency-table-img-left" src="{{ asset('/images/crypto/bitcoin.png') }}" alt="BTC">
                                <a href="{{ url('/converter/btc/'.strtolower($currency->currency_key).'/90') }}">BTC to {{ $currency->currency_key }}</a>
                              <img class="currency-table-img currency-table-img-right" src="{{ asset('/images/fiat/'.strtolower($currency->currency_key).'.png') }}" alt="{{ $currency->currency_key }}">
                            </li>
                            @endforeach
                          </ul>
                        </div>
                      </div>
                  </div>
                  <div class="col-12 col-lg-6">
                    <div><h3>CRYPTOCURRENCY CURRENCY TO BTC </h3>
                        <div class="block-currency">
                          <ul>
                            @foreach($cryptotobtc as $currency)
                            <li>
                              <img class="currency-table-img currency-table-img-left" src="{{ asset('/images/crypto/bitcoin.png') }}" alt="BTC">
                                <a href="{{ url('/converter/btc/'.strtolower($currency->currency_key).'/90') }}">BTC to {{ $currency->currency_key }}</a>
                              <img class="currency-table-img currency-table-img-right" src="{{ asset('/images/fiat/'.strtolower($currency->currency_key).'.png') }}" alt="{{ $currency->currency_key }}">
                            </li>
                            @endforeach
                          </ul>
                        </div>
                      </div>
                  </div>