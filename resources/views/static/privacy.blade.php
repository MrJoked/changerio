@extends('app')

@section('meta')
 				<title>@lang('meta.privacy_title') | @lang('meta.company_name') </title>
                <meta itemprop="name" content="{{ trans('meta.privacy_title') }}" />
                <meta itemprop="description" content="{{ trans('meta.privacy_description') }}" />
                <meta name="description" content="{{ trans('meta.privacy_description') }}" />
                <meta name="twitter:title" content="{{ trans('meta.privacy_title').' | '.trans('meta.company_name') }}" />
                <meta name="twitter:description" content="{{ trans('meta.privacy_description') }}" />
                <meta name="og:title" content="{{ trans('meta.privacy_title').' | '.trans('meta.company_name') }}" />
                <meta name="og:description" content="{{ trans('meta.privacy_description') }}" />
                <meta name="og:url" content="https://changerio.com/privacy" />
                <meta name="og:locale" content="{{ trans('meta.og_locale') }}" />

                <link rel="canonical" href="https://changerio.com/privacy" />
                <link rel="alternate" href="https://changerio.com/privacy" hreflang="x-default" />
                <link rel="alternate" hreflang="en" href="https://changerio.com/privacy" />
                <link rel="alternate" hreflang="fr" href="https://changerio.com/fr/privacy" />
@endsection


@section('header')
	@include('layouts.header')
@endsection

@section('mainclass', 'page')

@section('content')
<div class="container">
        <div class="blocks text-center">
            @include('layouts.banner', ['position' => 'privacy_top'])
        </div>
        <div class="row">
            <div class="col-12">
                <h3>@lang('content.privacy_title')</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12">
                <div class="content">
                    <h5>Bonus buy all the way around!</h5>
                    <p>I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized.</p>
                    <h5>Bonus buy all the way around!</h5>
                    <p>I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized.</p>
                    <h5>Bonus buy all the way around!</h5>
                    <p>I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized.</p>
                    <h5>Bonus buy all the way around!</h5>
                    <p>I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized.</p>
                    <h5>Bonus buy all the way around!</h5>
                    <p>I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized. I love the way this night cream makes my face feel. I clean my face good and them apply before bed. I apply this on my face and my neck. This cream dissolves fast into your face and leaves no greasy feeling. This cream makes my face feel so soft and moisturized.</p>
                </div>
            </div>
        </div>
        <div class="blocks text-center">
            @include('layouts.banner', ['position' => 'privacy_bottom'])
        </div>
    </div>
@endsection