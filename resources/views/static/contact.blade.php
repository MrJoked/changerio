@extends('app')


@section('meta')
 				<title>@lang('meta.contact_title') | @lang('meta.company_name')</title>
                <meta itemprop="name" content="@lang('meta.contact_title')" />
                <meta itemprop="description" content="@lang('meta.contact_description')" />
                <meta name="description" content="@lang('meta.contact_description')"/>
                <meta name="twitter:title" content="@lang('meta.contact_title') | @lang('meta.company_name')"/>
                <meta name="twitter:description" content="@lang('meta.contact_description')" />
                <meta name="og:title" content="@lang('meta.contact_title') | @lang('meta.company_name')"/>
                <meta name="og:description" content="@lang('meta.contact_description')" />
                <meta name="og:url" content="https://changerio.com/contact" />
                <meta name="og:locale" content="@lang('meta.og_locale')" />

                <link rel="canonical" href="https://changerio.com/contact" />
                <link rel="alternate" href="https://changerio.com/contact" hreflang="x-default" />
                <link rel="alternate" hreflang="fr" href="https://changerio.com/fr/contact" />
                <link rel="alternate" hreflang="en" href="https://changerio.com/contact" />
@endsection



@section('header')
	@include('layouts.header')
@endsection

@section('mainclass', 'page')

@section('content')
                <div class="container">
                    <div class="blocks text-center">
            			@include('layouts.banner', ['position' => 'contact_top'])
        			</div>
                    <div class="row">
                        <div class="col-12">
                            <h3>{{ trans('content.contact_title') }} </h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-lg-12">
                            <div class="content">
                                <h5>Have a question? We're here.</h5>
                                <p>Want to know more about advertising your business with Changerio?</p>
                                <p>Contact us for advertising, marketing and communication solutions and also for tie-ups and associations.</p>
                                <p>Email: info@changerio.com</p>

                            </div>
                        </div>
                    </div>
                    <div class="blocks text-center">
            			@include('layouts.banner', ['position' => 'contact_bottom'])
        			</div>
                </div>
@endsection