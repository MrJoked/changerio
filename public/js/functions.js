/************* Added at rebuilding at 11.04.2018 **************/

$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};

$return = "";
$return2 = "";

$( document ).ready(function() {
    //Submit convert
       $('input:submit').click(function(){
            from = $('#inputState1 .btn').text();
            to = $('#inputState2 .btn').text();
            sum = $('input.sum').val();

            from = from.match(/([A-Z]{1,6})\s-/i);
            from = from[1].toLowerCase();
            to = to.match(/([A-Z]{1,6})\s-/i);
            to = to[1].toLowerCase();
            window.location.href = document.location.origin+'/converter/'+from+'/'+to+'/'+sum;
       });
       /****** Changing button text after select ****/
    $( "#inputState2" ).on( "click", 'a.dropdown-item', function() {
        $('#inputState2 .btn').html($( this ).html() );
        $('#inputState2 .dropdown-menu').empty();
        $('#inputState2 .dropdown-menu').html($return2);
        //$('#inputState1 .dropdown-menu, #inputState2 .dropdown-menu').html($return);
    });
    $( "#inputState1" ).on( "click", 'a.dropdown-item', function() {
        $('#inputState1 .btn').html($( this ).html() );
        $('#inputState1 .dropdown-menu').empty();
        $('#inputState1 .dropdown-menu').html($return);
    });
});

// *********  S E A R C H
$(document).ready(function() {
      $('#search').keyup(function() {
        $('#search').search('#search_result');
      }); 
      $( "#inputState1" ).on( "keyup", '.search_internal', function() {
      //$('.search_internal').on('keyup', function() {
        $('.search_internal').search_internal();
      }); 
      $( "#inputState2" ).on( "keyup", '.search_internal2', function() {
        $('.search_internal2').search_internal();
      });
      $(document).click(function(){
          $('#search_result').fadeOut(300);
      });
     });




$.fn.search = function($div){
       var search = $(this).val();
       if (search !=''){
            $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
            $.post('/searchcurrency',
            {'search':search},
              function(result) 
              {  
                if (result) {
                    $search_result = $($div);
                    $autosuggest = $search_result.find('.autosuggest');
                    $search_result.fadeIn(300);
                    $autosuggest.html('');
                    for($i=0;$i<result[0].length;$i++){
                        $autosuggest.append('<li>'+result[0][$i]['currency_key']+' - '+result[0][$i]['currency_name']+'</li>');
                    }
                    $autosuggest.find('li').click(function() {
                        var searchword = $(this).html();
                        from = searchword.match(/([A-Z]{1,6})\s-/i);
                        from = from[1].toLowerCase();            
                        window.location.href = document.location.origin+'/converter/'+from;
                    });
                } else {
                    $autosuggest.html('No results');
                }                
              }
            );
        } 
};





$.fn.search_internal = function(){
       var searchEl = $(this);
       var search = $(this).val();
       
       if (search !=''){
            $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
            $.post('/searchcurrency',
            {'search':search},
              function(result) 
              {  
                if (result) {
                    $search_result = searchEl.closest('.dropdown-menu');
                    $search_result.find('a.dropdown-item').remove();

                    for($i=0;$i<result[0].length;$i++){
                        $item = result[0][$i];
                        if(result[1]>$i && result[1]>0){
                          path = '/images/fiat/'+$item['currency_key'].toLowerCase()+'.png';
                        }
                        else{
                          path = '/images/crypto/'+$item['currency_icon_name'].toLowerCase()+'.png';
                        }
                        $search_result.append('<a class="dropdown-item"><img class="mr-3" src="'+path+'">'+$item['currency_key']+' - '+$item['currency_name']+'</a>');
                    }
                } else {
                    $autosuggest.html('No results');                  
                }                
              }
            );
        } else {
        }
};



