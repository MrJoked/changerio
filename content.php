<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Content Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    
    'home_main_title' => 'CRYPTOCURRENCY CONVERTER',
    'home_sub_title' => 'Crypto to Fiat, Fiat to Crypto, Crypto to Crypto Currency Converter',
    'home_button_convert' => 'Please, press convert button',
    'home_convert_guide' => 'Cryptocurrencies rates updated every 5 minutes & Fiat Currency rates updated every 1 hour.',
    'home_live_title' => 'Live Conversion Rate',
    'home_recent_title' => 'RECENTLY CONVERTED',
    'home_crypto_title' => 'CRYPTOCURRENCIES',
    'home_fiat_title' => 'FIAT CURRENCIES',
    'home_crypto_more' => 'More Cryptocurrencies',
    'home_fiat_more' => 'More Fiat Currencies',
    'converter_main_title' => 'CRYPTOCURRENCY CONVERTER',
    'converter_sub_title' => 'Crypto to Fiat, Fiat to Crypto, Crypto to Crypto Currency Converter',
    'converter_convert_guide' => 'Cryptocurrencies rates updated every 5 minutes & Fiat Currency rates updated every 1 hour.',
    'converter_popular_title' => 'POPULAR CONVERSION AMOUNTS',
    'converter_recent_title' => 'RECENTLY CONVERTED',
    'converter_fiat_currency' => 'FIAT CURRENCY',
    'converter_crypto_currency' => 'CRYPTOCURRENCY',
    'cryptocurrencies_title' => 'Cryptocurrencies List',
    'fiatcurrencies_title' => 'Fiat Currencies List',
    'fiat_converter_title' => 'CURRENCY CONVERTER',
    'ico_title' => 'Popular Ico List',
    'ico_name' => 'Name',
    'ico_description' => 'Description',
    'ico_start' => 'Start Date',
    'ico_end' => 'End Date',
    'ico_link' => 'Links',
    'currency_title' => 'Cryptocurrency prices',
    'currency_title_btc' => 'Cryptocurrency price in Bitcoin (BTC)',
    'currency_title_eth' => 'Cryptocurrency price in Ethereum (ETH)',
    'currency_title_usd' => 'Cryptocurrency price in US Dollar (USD)',
    'currency_title_eur' => 'Cryptocurrency price in Euro (EUR)',
    'currency_title_gbp' => 'Cryptocurrency price in British Pound (GBP)',
    'currency_title_inr' => 'Cryptocurrency price in Indian Rupee (INR)',
    'currency_name' => 'Name',
    'currency_marketcap' => 'Market Cap',
    'currency_price' => 'Price',
    'currency_volume' => 'Volume',
    'currency_supply' => 'Maximum Supply',
    'currency_change' => 'Change',
    'common_hour' => 'Hour',
    'common_hours' => 'Hours',
    'common_minute' => 'Minute',
    'common_minutes' => 'Minutes',
    'common_to' => 'to',
    'common_ago' => 'ago',
    'common_home' => 'Home',
    'title_converter' => 'Converter',
    'subtitle_converter' => 'Conversion Rates',
    'title_convert' => 'Conversion',
    'subtitle_convert' => 'Conversion Rates',
    'title_calculator' => 'Calculator',
    'subtitle_calculator' => 'Calculator',
    'title_exchange' => 'Exchange',
    'subtitle_exchange' => 'Exchange Rates',
    'title_converter_com' => 'Converter',
    'subtitle_converter_com1' => 'Conversion Rates',
    'subtitle_converter_com2' => 'Exchange Rates',
    'terms_title' => 'Terms of Use',
    'privacy_title' => 'Privacy Policy',
    'contact_title' => 'Contact Us',
    'disclaimer_title' => 'Disclaimer',
    'disclaimer_text' => 'None of the content or information provided in our newsletter or on this site should be considered investment or financial advice. Please do your own research before participating or investing in an ICO. The content provided or linked on this site is for informational purposes only.',
    'price_chart' => 'Price Chart'
  ];
  